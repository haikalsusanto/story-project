from django.db import models

# Create your models here.

class Course(models.Model):
    subject     = models.CharField('Subject', max_length=200)
    credit      = models.IntegerField('Credit')
    lecturer    = models.CharField('Lecturer', max_length=200)
    class_room  = models.CharField('Classroom', max_length=200)
    term        = models.IntegerField('Term',)
    description = models.TextField('Description',blank=True)

    def __str__(self):
        return self.subject
