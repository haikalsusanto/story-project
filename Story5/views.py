from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Course
from .forms import CourseForm

# Create your views here.

def courseList(request):
    if request.method == "POST":
        Course.objects.get(id=request.POST['id']).delete()
        return redirect('/mycourses/')
    list_course = Course.objects.all()
    return render(request, 'my_courses.html', {'list_course': list_course})

def addCourse(request):
    submitted = False
    if request.method == 'POST':
        form = CourseForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('?submitted=True')
    else:
        form = CourseForm()
        if 'submitted' in request.GET:
            submitted = True
    return render(request,'add_course.html', {'form':form, 'submitted':submitted})

def courseDetails(request, index):
    course = Course.objects.get(pk=index)
    content = {
        'course' : course
    }
    return render(request, 'course_details.html', content)
