from django.urls import path
from . import views

app_name= "Story5"
urlpatterns = [
    path('', views.courseList, name='courseList' ),
    path('coursedetails/<int:index>/', views.courseDetails, name='courseDetails'),
    path('addcourse/', views.addCourse, name='addCourse')
]
