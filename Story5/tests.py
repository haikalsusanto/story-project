from django.test import TestCase, Client
from django.urls import resolve
from .views import courseList, addCourse, courseDetails
from .models import Course


# Create your tests here.
class TestCourse(TestCase):
	def test_subject_url_is_exist(self):
		response = Client().get('/mycourses/')
		self.assertEqual(response.status_code, 200)

	def test_subject_index_func(self):
		found = resolve('/mycourses/')
		self.assertEqual(found.func, courseList)

	def test_subject_using_template(self):
		response = Client().get('/mycourses/')
		self.assertTemplateUsed(response, 'my_courses.html')

class TestAddCourse(TestCase):
	def test_add_subject_url_is_exist(self):
		response = Client().get('/mycourses/addcourse/')
		self.assertEqual(response.status_code, 200)

	def test_add_subject_index_func(self):
		found = resolve('/mycourses/addcourse/')
		self.assertEqual(found.func, addCourse)

	def test_add_subject_using_template(self):
		response = Client().get('/mycourses/addcourse/')
		self.assertTemplateUsed(response, 'add_course.html')

class TestCourseModel(TestCase):
	def subject_model_create_new_object(self):
		Course.objects.create(subject="sdsd", credit=3, lecturer="sdsd", class_room="A1.07", term=2 )
		self.assertEqual(Course.objects.all().count(), 1)

