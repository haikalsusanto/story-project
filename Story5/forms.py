from .models import Course
from django.forms import ModelForm

class CourseForm(ModelForm):
    required_css_class = "required"
    class Meta:
        model = Course
        fields = "__all__"