from django.shortcuts import render, redirect
from .models import Activity, Member
from .forms import ActivityForm, MemberForm

# Create your views here.

def activityList(request):
    activities = Activity.objects.all()
    members = Member.objects.all()
    if request.method == "POST":
        Member.objects.get(id=request.POST['id']).delete()
        return redirect('/activity/')
    
    return render(request, 'activity_list.html', {'activities': activities, 'members': members})

def addActivity(request):
    if request.method == 'POST':
        form = ActivityForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/activity/')
    else:
        form = ActivityForm()
    return render(request, "add_activity.html", {'form': form})

def addMember(request, index):
    if request.method == 'POST':
        form = MemberForm(request.POST)
        if form.is_valid():
            member = Member(activity=Activity.objects.get(id=index), member_name=form.data['member_name'])
            member.save()
            return redirect('/activity/')
    else:
        form = MemberForm()
        
    return render(request, 'add_member.html', {'form': form})
