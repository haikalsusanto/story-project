from django.forms import ModelForm
from .models import Activity, Member

class ActivityForm(ModelForm):
	required_css_class = "required"
	class Meta:
		model = Activity
		fields = '__all__'
		
class MemberForm(ModelForm):
	class Meta:
		model = Member
		fields = ['member_name']
