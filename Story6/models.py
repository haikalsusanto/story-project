from django.db import models

# Create your models here.

class Activity(models.Model):
    name = models.CharField("Activity Name" , max_length=200)


class Member(models.Model):
    activity = models.ForeignKey("Activity",on_delete=models.CASCADE, null=True, blank=True)
    member_name = models.CharField("Name", max_length=200)

