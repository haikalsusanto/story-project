from django.urls import path
from . import views

app_name = "Story6"
urlpatterns = [
    path('', views.activityList, name="activityList"),
    path('addActivity/', views.addActivity, name="addActivity"),
    path('addMember/<int:index>/', views.addMember ,name="addMember"),
]