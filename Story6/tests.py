from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import Story6Config
from .views import activityList,addActivity, addMember
from .models import Activity, Member

# Create your tests here.

class TestActivityList(TestCase):
	def test_activity_url_is_exist(self):
		response = Client().get('/activity/')
		self.assertEqual(response.status_code, 200)

	def test_activity_index_func(self):
		found = resolve('/activity/')
		self.assertEqual(found.func, activityList)

	def test_activity_using_template(self):
		response = Client().get('/activity/')
		self.assertTemplateUsed(response, 'activity_list.html')

	def test_form_activity_works(self):
		activity = Activity.objects.create(name="abc")
		member = Member.objects.create(activity=activity, member_name='abc')
		response = Client().post('/activity/', data={'id':1})
		self.assertEqual(response.status_code, 302)

	def test_activity_model_create_new_object(self):
		activity = Activity(name="xyz")
		activity.save()
		self.assertEqual(Activity.objects.all().count(), 1)


class TestAddMember(TestCase):
	def setUp(self):
		activity = Activity(name="xyz")
		activity.save()

	def test_regist_url_is_exist(self):
		response = Client().post('/activity/addMember/1/', data={'member_name':'samsudinkomarudin'})
		self.assertEqual(response.status_code, 302)

	def test_regist_using_template(self):
		response = Client().get('/activity/addMember/1/')
		self.assertTemplateUsed(response, 'add_member.html')


class TestAddActivity(TestCase):
	def test_add_activity_url_is_exist(self):
		response = Client().get('/activity/addActivity/')
		self.assertEqual(response.status_code, 200)
        
	def test_add_activity_using_template(self):
		response = Client().get('/activity/addActivity/')
		self.assertTemplateUsed(response, 'add_activity.html')

	def test_form_activity_works(self):
		response = Client().post('/activity/addActivity/', data={'name':'whayolo'})
		self.assertEqual(response.status_code, 302)


