from django.urls import path
from . import views

app_name= "Story4"
urlpatterns = [
    path('', views.index, name='index' ),
    path('what/', views.what, name='what' )
]